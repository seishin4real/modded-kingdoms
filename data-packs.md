## Not Vanilla Tweaks
* [illager-fortresses](https://pcminecraft-mods.com/illager-fortresses-data-pack-minecraft-1-16-3/)
* [ships-oceans](https://pcminecraft-mods.com/ships-oceans-data-pack-minecraft-1-16-3/)

## [Vanilla Tweaks - RESOURCE PACKS](https://vanillatweaks.net/picker/resource-packs/)
### Aesthetic:
* Bushy Leaves
* Different Stems
* Red Iron Golem Flowers
* Colorful Enchanting Table Particles
* Unique Dyes
* Golden Crown
* Bed Icons
  
### Terrain:
* Circular Sun and Moon
* Shorter Tall Grass
* Shorter Grass
  
### Lower And Sides:
* Lower Grass
* Lower Mycelium
* Lower Paths
* Lower Podzol
* Lower Snow
* Lower Warped Nylium
* Lower Crimson Nylium

### Connected Textures:
* Variated Connected Bookshelves

### Peace And Quiet:
* Quieter Rain
* Quieter Dispensers and Droppers
* Quieter Minecarts
* Quieter Villagers
* Quieter Fire
* Quieter Nether Portals
* Quieter Pistons
* Quieter Cows

### Utility:
* Ore Borders
* Visual Infested Stone Items
* Age 25 Kelp
* Clear Banner Patterns
* Sticky Piston Sides
* Directional Hoppers
* Directional Dispensers & Droppers
* Diminishing Tools
* Better Observers
* Redstone Power Levels
* Visual Honey Stages
* Visual Note Block Pitch

### Unobtrusive:
* Unobtrusive Rain
* Unobtrusive Particles
* Unobtrusive Scaffolding
* Lower Shield
* Lower Fire
* Transparent Pumpkin
* Borderless Stained Glass
* Clean Glass

### Hud:
* Wither Hearts
* Ping Color Indicator
  
### Gui:
* Smoother Font
* Dark UI
* Numbered Hotbar

### Fun:
* Wandering Xisuma
* Beeralis

## [Vanilla Tweaks - DATA PACKS](https://vanillatweaks.net/picker/datapacks/)
### Survival:
* armor statues v2.8.1
* unlock all recipes v2.0.0
* fast leaf decay v2.0.3
* multiplayer sleep v2.5.2
* custom nether portals v2.3.2
* durability ping v1.1.0
* nether portal coords v1.1.0
* coordinates hud v1.2.0
* villager workstation highlights v1.1.0
### Items:
* redstone rotation wrench v1.1.1
* terracotta rotation wrench v1.1.1
* armored elytra v1.0.3
* player head drops v1.1.0
### Mobs:
* anti enderman grief v1.1.0
* double shulker shells v1.3.0
* dragon drops v1.3.0
* larger phantoms v1.2.1
* more mob heads v2.8.2
* silence mobs v1.1.0
### Teleportation:
* homes v1.4.1
* tpa v2.3.0
* back v1.2.0
### Utilities:
* custom villager shops v1.2.0
* spectator night vision v1.1.0
* spectator conduit power v1.1.0
### Hermitcraft:
* gem villagers v4.3.2
* treasure gems v1.2.1
* wandering trades v1.4.0
### Experimental:
* chunk loaders v1.0.1

## [Vanilla Tweaks - CRAFTING TWEAKS](https://vanillatweaks.net/picker/crafting-tweaks/)
### Quality Of Life:
* Back to Blocks
* Dropper to Dispenser
* Rotten Flesh to Leather
* Charcoal to Black Dye
* Coal to Black Dye
* Powder to Glass
* Blackstone Cobblestone
* Straight to Shapeless
* Universal Dyeing
* Sandstone Dyeing
### More Blocks:
* More Trapdoors
* More Bark
* More Bricks
* More Stairs
### Craftables:
* Craftable Gravel
* Craftable Horse Armor
* Craftable Notch Apples
* Craftable Name Tags
* Craftable Blackstone
### Unpackables:
* Unpackable Ice
* Unpackable Nether Wart
* Unpackable Wool
