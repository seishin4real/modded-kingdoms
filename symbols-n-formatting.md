§k obfuscated  
§l bold  
§m strikethrough  
§n underline  
§o italic  
§r reset  

§0 black  
§1 deep blue  
§2 green  
§3 cyan  
§4 dark red  
§5 purple  
§6 orange  
§7 light gray  
§8 dark gray  
§9 meh blue  
§a light green  
§b light cyan  
§c red  
§d light purple / pink  
§e yellow  
§f white  

☢	☢
☣	☣
⋆	⋆
⍟	⍟
★	★
☆	☆
✡	✡
✩	✩
✪	✪
✰	✰
•	•
◘	◘
◦	◦
✓	✓
✔	✔
☑	☑
©	©
™	™
°	°
˚	˚
♂	♂
♀	♀
♡	♡
♥	♥
❣	❣
❤	❤
❥	❥
❦	❦
✝	✝
✟	✟
✞	✞
✝	✝
☩	☩
☦	☦
☨	☨
☩	☩
✚	✚
✠	✠
♩	♩
♪	♪
♫	♫
♬	♬
♭	♭
♮	♮
♯	♯
☮	☮
®	®
☠	☠
❄	❄
❅	❅
❆	❆
☺	☺
☻	☻
☹	☹
☿	☿
♀	♀
♁	♁
♂	♂
♃	♃
♄	♄
♅	♅
♆	♆
♇	♇
☎	☎
☏	☏
✆	✆
☼	☼
☀	☀
☉	☉
☁	☁
♈	♈
♉	♉
♊	♊
♋	♋
♌	♌
♍	♍
♎	♎
♏	♏
♐	♐
♑	♑
♒	♒
♓	♓
♠	♠
♣	♣
♥	♥
♦	♦
♛	♛
♚
