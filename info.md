# TPA
**ID**|**TPA**|**GATEWAYS**
:-----:|:-----:|:-----:
SERVER|`-`|`GA` `GA`
seishin4real|`1`|`CY` `OR`
SamTHStefan|`3`|`??` `??`
KubaCross|`4`|`??` `??`
KacperQuad1|`?`|`??` `??`
Leonikx|`?`|`??` `??`
Marceloxii|`?`|`??` `??`
Archon|`?`|`??` `??`

# Ancient Portals
## COLORS
CL|Color|CL|Color|CL|Color|
:-:|:------------:|:--:|:------------:|:--:|:-----:
`BK` | black        | `BL` | blue         | `BR` | brown
`CY` | cyan         | `GA` | gray         | `GE` | green
`LB` | light blue   | `LG` | light gray   | `LM` | lime
`MA` | magenta      | `OR` | orange       |    |
`PI` | pink         | `PU` | purple       |    |
`RE` | red          | `WH` | white        | `YE` | yellow
___
## Address format: `## # ###` 
### => `##` - owner `#` - dimension `###` - place
___

Dimensions|Symbol|Rune|
:-----:|:-----:|:-----:
overworld|`O`|`LM`
nether   |`N`|`RE`
end      |`E`|`BK`

## SERVER ADRESSES
Dimension|Runes|Desc|Coordinates|
:-----:|:-----:|:-----:|:-----:
`O`|`LM` `LM` `LM`|nowy spawn|
`O`|`GA` `GA` `GA`|stary spawn|
`N`|`RE` `RE` `RE`|blaze spawner|
`E`|`BK` `BK` `BK`|random gateway|(-1727, 57, 380)
`E`|`GE` `PU` `BK`|shadow forest|(1382, 57, -2806)
`E`|`BR` `BL` `LB`|field of light|(4747, 66, -2157)
`E`|`PI` `YE` `LG`|newar end city|(3640, 66, -902)

example - first address would be: `GA GA` (server) `LM` (overworld) `LM LM LM` (place)
___
___
# Armor Stand Book:
* write something in a book and sign it `Statues`

# Silent mobs:
* use `silent me` name tag

# Useful Commands:
* `/trigger sethome` (save home location)
* `/trigger home` (tp home)
* `/trigger namehome set <PID>` (name home #PID while holding a named nametag)
* `/trigger tpa set <PID>]` (request tp to player)
* `/trigger back` (tp to last tp point of origin)
* `/trigger nc_inNether` (get overworld coords when in nether)
* `/trigger nc_inOverworld` (get nether coords when in overworld)



# REI Search Modes:
* Tooltip: `#text` -  Non-case sensitive, searching via tooltip, for example #protection to search for protection books.
* Mod: `@modname` -  Non-case sensitive, searching via mod, for example @materialisation to search for items added by materialisation.
* Or: `|` -  Or, for example apple|$logs to search for apples or logs.
* Quotes: `""` -  Allowing spaces, for example "#protection iii" to search with spaces.
* Tags: `$tag` -  Non-case sensitive, searching via tags, for example $logs to search for logs.
* Regex: `r/regex/` -  Case sensitive, searching via regex, for example r/(Golden )?Apple) to search for golden apples or apples.

