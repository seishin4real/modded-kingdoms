# MODS

## CLIENT ONLY (so kind of optional/files)
* [Anti-ghost](https://www.curseforge.com/minecraft/mc-mods/antighost/files) no more ghost blocks
* [Better Beds](https://www.curseforge.com/minecraft/mc-mods/better-beds/files) 3d beds
* [Better Sodium Video Settings Button](https://www.curseforge.com/minecraft/mc-mods/better-sodium-button/files) classic video settings with button to sodium ones
* [Disable Custom Worlds Advice](https://www.curseforge.com/minecraft/mc-mods/fabric-disable-custom-worlds-advice/files) skips annoying intermediate screen
* [Enchantment Lore](https://www.curseforge.com/minecraft/mc-mods/enchantment-lore/files) enchantment books have stories
* [Inventory Control Tweaks](https://www.curseforge.com/minecraft/mc-mods/inventory-control-tweaks/files)
* [Inventory HUD+](https://www.curseforge.com/minecraft/mc-mods/inventory-hud-forge/files) armor/effects info in the hud
* [Light Overlay](https://www.curseforge.com/minecraft/mc-mods/light-overlay/files) light levels for mob-proofing
* [Minihud](https://www.curseforge.com/minecraft/mc-mods/minihud/files) configurable F3 like info
* [Ok Zoomer](https://www.curseforge.com/minecraft/mc-mods/ok-zoomer/files) in-game zoom
* [Recently Used](https://www.curseforge.com/minecraft/mc-mods/recently-used/files) new tab in inventory
* [Roughly Enough Items (REI/files)](https://www.curseforge.com/minecraft/mc-mods/roughly-enough-items/files) items finder/helper
* [Time To Live](https://www.curseforge.com/minecraft/mc-mods/time-to-live/files) tnt/creeper timer
* [ToroHealth Damage Indicators](https://www.curseforge.com/minecraft/mc-mods/torohealth-damage-indicators/files) mobs health
* [XYKey](https://www.curseforge.com/minecraft/mc-mods/xykey/files) quickly paste coordinates into chat
  ### Useful in the background
  * [Cloth Config API](https://www.curseforge.com/minecraft/mc-mods/cloth-config/files) util lib
  * [Hydrogen](https://github.com/CaffeineMC/hydrogen-fabric/releases/files) performance+
  * [Mod Menu](https://minecraft.curseforge.com/projects/modmenu/files) menu for mods
  * [Not Enough Animations](https://www.curseforge.com/minecraft/mc-mods/not-enough-animations/files)
  * [Sodium](https://www.curseforge.com/minecraft/mc-mods/sodium/files) 3x the fps
  * [TRansliterationLib](https://www.curseforge.com/minecraft/mc-mods/transliterationlib/files) util lib
  * [Grabcraft-Litematic](https://www.curseforge.com/minecraft/mc-mods/grabcraftlitematic/files) import schematics into litematica
  * [Litematica](https://www.curseforge.com/minecraft/mc-mods/litematica/files) building utility
  * [WorldEdit CUI](https://www.curseforge.com/minecraft/mc-mods/worldeditcui-fabric/files) [OP only]

### Enhancements: NEW BLOCKS & ITEMS
* [Ancient Gateways](https://www.curseforge.com/minecraft/mc-mods/ancient-gateways/files)
* [Automated Crafting](https://www.curseforge.com/minecraft/mc-mods/automated-crafting/files)
* [BackSlot](https://www.curseforge.com/minecraft/mc-mods/backslot/files) back and belt slots
* [Bedspreads](https://www.curseforge.com/minecraft/mc-mods/bedspreads-fabric/files) add banners to beds
* [Better Barrels](https://www.curseforge.com/minecraft/mc-mods/better-barrels/files) early-game shulker boxes
* [Better Than Mending](https://www.curseforge.com/minecraft/mc-mods/better-than-mending/files) mending with xp [sneak + right click]
* [Biome Locator](https://www.curseforge.com/minecraft/mc-mods/biome-locator/files) 
* [Concrete Plus](https://www.curseforge.com/minecraft/mc-mods/concrete-plus/files) more concrete items
* [Cosmetic Armor Fabric](https://www.curseforge.com/minecraft/mc-mods/cosmetic-armor-fabric/files)
* [Cursed Table](https://www.curseforge.com/minecraft/mc-mods/cursed-table/files)
* [Dyetherite](https://www.curseforge.com/minecraft/mc-mods/dyetherite/files)
* [Emotecraft](https://www.curseforge.com/minecraft/mc-mods/emotecraft/files) emotions for player avatar
* [Golden Hopper](https://www.curseforge.com/minecraft/mc-mods/golden-hopper/files) filter hopper
* [Harvest Scythes](https://www.curseforge.com/minecraft/mc-mods/harvest-scythes/files)
* [Hoppers Plus](https://www.curseforge.com/minecraft/mc-mods/hopper)
* [MC Dungeons Armors](https://www.curseforge.com/minecraft/mc-mods/mcda/files)
* [MC Dungeons Weapons](https://www.curseforge.com/minecraft/mc-mods/mcdw/files)
* [Netherite Plus](https://www.curseforge.com/minecraft/mc-mods/netherite-plus-mod//files) more netherite items
* [Not Enough Creativity](https://www.curseforge.com/minecraft/mc-mods/not-enough-creativity/files) creative mode on steroids [OP only]
* [Ring Of The Enderchest](https://www.curseforge.com/minecraft/mc-mods/ring-of-the-enderchest-fabric/files) access enderchest from inventory
* [Shelf](https://www.curseforge.com/minecraft/mc-mods/shelf/files)
* [Simple Angel Ring](https://www.curseforge.com/minecraft/mc-mods/simple-angel-ring/files) creative flying in survival
* [Spawn Lanterns](https://www.curseforge.com/minecraft/mc-mods/spawn-lanterns/files)

### Enhancements: QUALITY OF LIFE
* [All Stackable](https://www.curseforge.com/minecraft/mc-mods/all-stackable//files)
* [Edit Sign](https://www.curseforge.com/minecraft/mc-mods/edit-sign/files)
* [Haema](https://www.curseforge.com/minecraft/mc-mods/haema/files) vampires
* [Inventory Sorting](https://www.curseforge.com/minecraft/mc-mods/inventory-sorting/files)
* [Quick Shulker](https://www.curseforge.com/minecraft/mc-mods/quick-shulker/files) access shulker boxes from inventory
* [Reroll](https://www.curseforge.com/minecraft/mc-mods/reroll/files) change table enchantment for a cost
* [ShulkerBoxTooltip](https://www.curseforge.com/minecraft/mc-mods/shulkerboxtooltip/files) shulker box contents in a tooltip

### Enhancements UX and WORLD GEN
* [BetterEnd](https://www.curseforge.com/minecraft/mc-mods/betterend/files) more biomes
* [BetterNether](https://www.curseforge.com/minecraft/mc-mods/betternether/files) more biomes
* [Biome Makeover](https://www.curseforge.com/minecraft/mc-mods/biome-makeover/files) more biomes
* [Cinderscapes](https://www.curseforge.com/minecraft/mc-mods/cinderscapes/files) more biomes
* [Colored Slime Blocks](https://www.curseforge.com/minecraft/mc-mods/colored-slime-blocks/files) more mobs
* [Immersive Portals](https://www.curseforge.com/minecraft/mc-mods/immersive-portals-mod/files)
* [Mining Dimensions](https://www.curseforge.com/minecraft/mc-mods/mining-dimensions-fabric/files) more dimensions
* [Trade](https://www.curseforge.com/minecraft/mc-mods/trade/files) player to player trading
* [Visual Overhaul](https://www.curseforge.com/minecraft/mc-mods/visual-overhaul/files) few items ui improvement

### APIs:
* [Architectury API](https://www.curseforge.com/minecraft/mc-mods/architectury-fabric/files) util lib
* [Auto Config API](https://www.curseforge.com/minecraft/mc-mods/auto-config-updated-api/files) util lib
* [Curios API](https://www.curseforge.com/minecraft/mc-mods/curios-fabric/files) utils lib
* [Fabric API](https://www.curseforge.com/minecraft/mc-mods/fabric-api/files) util lib
* [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin/files) util lib
* [Fabrication](https://www.curseforge.com/minecraft/mc-mods/fabrication/files) tweaks
* [Krypton](https://www.curseforge.com/minecraft/mc-mods/krypton/files) performance
* [Lithium](https://www.curseforge.com/minecraft/mc-mods/lithium/files) performance
* [Malilib](https://www.curseforge.com/minecraft/mc-mods/malilib/files) util lib
* [Phosphor](https://www.curseforge.com/minecraft/mc-mods/phosphor/files) performance
* [Physics Mod](https://www.curseforge.com/minecraft/mc-mods/physics-mod/files) better physics

### Mod/Admin Tools:
* [WorldEdit](https://www.curseforge.com/minecraft/mc-mods/worldedit/files) [OP only]
